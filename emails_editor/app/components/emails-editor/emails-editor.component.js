'use strict'

angular.module('emailsEditor').component('emailsEditor', {
  templateUrl: 'components/emails-editor/emails-editor.template.html',
  controller: function EmailsEditorController($scope, regexService) {

    $scope.emails = this.emails;

    let getEmailsFromText = (text) => {
      let emails = [];

      if (text !== '') {
        let parsedToLines = text.split(regexService.lineSeparatorsRegex).filter(x => x.length !== 0 && x.trim());

        parsedToLines.forEach(textLine => {
          let parsedTextLine = textLine.split(regexService.inlineSeparatorsRegex).filter(x => x.length !== 0 && x.trim());
          let i = 0;

          do {
            if (regexService.emailRegex.test(parsedTextLine[i])) {
              emails.push(parsedTextLine[i]);
              i++;
            } else {
              let incorrectString = '';

              if (i > 0) {
                let startIncorrectIndex = textLine.lastIndexOf(parsedTextLine[i - 1]) + parsedTextLine[i - 1].length;

                incorrectString = textLine.substring(startIncorrectIndex);
              } else {
                incorrectString = textLine;
              }

              emails.push(incorrectString.trim());

              break;
            }
          } while (i < parsedTextLine.length);
        });
      }

      return emails;
    };

    let watchEmails = (newEmails, oldEmails) => {
      let isInit = newEmails == oldEmails;

      let newEmailsFiltered = newEmails.filter((item, i, arr) => {
        return !!item && arr.indexOf(item) === i && !regexService.separatorsOnlyRegex.test(item);
      });

      let addedTexts = isInit ? newEmailsFiltered : newEmailsFiltered.filter((item) => {
        return oldEmails.indexOf(item) < 0;
      });

      if (addedTexts.length > 0) {
        addedTexts.forEach((addedText, index) => {
          let parsedEmails = getEmailsFromText(addedText).filter((parsedEmail, i, parsedEmails) => {
            return parsedEmails.indexOf(parsedEmail) === i &&
                   (newEmailsFiltered.indexOf(parsedEmail) < 0 ||
                   newEmailsFiltered.indexOf(parsedEmail) === newEmailsFiltered.indexOf(addedText));
          });

          if (parsedEmails.length > 0) {
            Array.prototype.splice.apply(newEmailsFiltered, [newEmailsFiltered.indexOf(addedText), parsedEmails.length].concat(parsedEmails));
          } else {
            newEmailsFiltered.splice(newEmailsFiltered.indexOf(addedText), 1);
          }
        });
      }

      Array.prototype.splice.apply(newEmails, [0, newEmails.length].concat(newEmailsFiltered));
    };

    $scope.$watchCollection('emails', watchEmails);
  },
  bindings: {
    emails: '='
  }
});
