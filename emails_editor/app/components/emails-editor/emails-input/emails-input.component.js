'use strict'

angular.module('emailsEditor').component('emailsInput', {
  templateUrl: 'components/emails-editor/emails-input/emails-input.template.html',
  controller: function EmailsInputController($scope, regexService) {
    this.areaValue = '';

    this.onBackspace = () => {
      if (this.areaValue === '') {
        this.emails.pop();
      }
    };

    let addEmails = () => {
      if (!!this.areaValue) {
        this.emails.push(this.areaValue);
        this.areaValue = '';
      }
    };

    this.onBlur = () => addEmails();

    this.onEnter = () => addEmails();

    this.onPaste = event => {
      event.preventDefault();

      let pastedData = event.clipboardData.getData('Text');
      let text = this.areaValue.slice(0, event.target.selectionStart) + pastedData + this.areaValue.slice(event.target.selectionEnd);

      this.emails.push(text);

      this.areaValue = '';
    };
  },
  bindings: {
    emails: '=',
  }
});
