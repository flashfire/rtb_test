'use strict'

angular.module('emailsEditor').component('emailBlock', {
  templateUrl: 'components/emails-editor/email-block/email-block.template.html',
  controller: function EmailsBlockController($scope, regexService) {
    this.valid = regexService.emailRegex.test(this.email);

    this.deleteBlock = () => {
      let index = this.emails.indexOf(this.email);

      this.emails.splice(index, 1);
    };
  },
  bindings: {
    email: '<',
    emails: '='
  }
});
