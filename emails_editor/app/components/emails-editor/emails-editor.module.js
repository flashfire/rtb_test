'use strict';

angular.module('emailsEditor', []).service('regexService', function() {
  this.emailRegex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  this.separatorsOnlyRegex = /[\s,\t\n\r]$/;
  this.lineSeparatorsRegex = /[,?\n?\r?]/;
  this.inlineSeparatorsRegex = /[\s?\t?]+/;
});
