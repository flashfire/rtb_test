'use strict'

angular.module('editorExampleApp').component('emailsEditorExample', {
  templateUrl: 'components/emails-editor-example/emails-editor-example.template.html',
  controller: function EmailsEditorExampleController($scope, emailGeneratorService) {
    this.emails = [];

    this.addEmails = () => {
      let randomMail = emailGeneratorService.generate();

      this.emails.push(randomMail);
    };

    this.getEmailsCount = () => {
      let message = this.emails.length;

      alert(message);
    };
  },
  bindings: {
    boardName: '@'
  }
});
