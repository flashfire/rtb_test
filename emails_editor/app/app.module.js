'use strict';

angular.module('editorExampleApp', [
  'emailsEditor'
]).service('emailGeneratorService', function() {
  this.generate = () => Math.random().toString(36).replace(/[^a-z]+/g, '').substring(0, 5) + '@example.com';
}).constant('KEY_CODE', {
  'ENTER': 13,
  'BACKSPACE': 8
});
