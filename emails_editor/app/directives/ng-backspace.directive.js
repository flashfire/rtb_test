'use strict'

angular.module('editorExampleApp').directive('ngBackspace', function (KEY_CODE) {
       return function (scope, elements, attrs) {
           elements.bind('keydown keypress', function (event) {
               if (event.which === KEY_CODE.BACKSPACE) {
                   scope.$apply(function () {
                       scope.$eval(attrs.ngBackspace);
                   });
               }
           });
       };
   });
