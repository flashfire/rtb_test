'use strict'

angular.module('editorExampleApp').directive('ngEnterComma', function (KEY_CODE) {
       return function (scope, elements, attrs) {
           elements.bind('keydown keypress', function (event) {
               if (event.which === KEY_CODE.ENTER || event.key === ',') {
                   scope.$apply(function () {
                       scope.$eval(attrs.ngEnterComma);
                   });
                   event.preventDefault();
               }
           });
       };
   });
